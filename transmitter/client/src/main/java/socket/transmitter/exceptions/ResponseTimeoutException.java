package socket.transmitter.exceptions;

public class ResponseTimeoutException extends RuntimeException {

    public ResponseTimeoutException(String message) {
        super(message);
    }

    public ResponseTimeoutException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
