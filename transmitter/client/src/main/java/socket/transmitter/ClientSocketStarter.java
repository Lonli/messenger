package socket.transmitter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.connection.Connection;
import socket.transmitter.connection.ConnectionHolder;
import socket.transmitter.response.ClientResponseListener;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import static socket.transmitter.exceptions.messages.MessageConstants.*;

public class ClientSocketStarter {

    private static final Logger logger = LoggerFactory.getLogger(ClientSocketStarter.class);

    public void start(String host, int port) {
        logger.info(CONNECTING_TO_THE_SERVER);
        try (Socket socket = new Socket(host, port)) {
            logger.info(CONNECTION_TO_THE_SERVER_COMPLETED);

            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

            Connection connection = new Connection(socket, inputStream, outputStream);
            ConnectionHolder.getInstance().setCollection(connection);
            new ClientResponseListener(connection).run();

        } catch (IOException e) {
            logger.error(CONNECTION_REFUSED, e);
        }
    }

}
