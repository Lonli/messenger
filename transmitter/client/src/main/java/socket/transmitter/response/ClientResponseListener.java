package socket.transmitter.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.connection.Connection;
import socket.transmitter.exceptions.ConnectionException;
import socket.transmitter.exceptions.NoDataReceivedException;
import socket.transmitter.socket.response.SocketResponse;

import static java.lang.String.format;
import static socket.transmitter.exceptions.messages.MessageConstants.CLOSED_CONNECTION_WITH;
import static socket.transmitter.exceptions.messages.MessageConstants.NO_RESPONSE_FROM;

public class ClientResponseListener implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(ClientResponseListener.class);

    private final Connection connection;
    private final ClientResponseHandler handler;

    public ClientResponseListener(Connection connection) {
        this.connection = connection;
        this.handler = new ClientResponseHandler(); // ToDo dependency injection;
    }

    @Override
    public void run() {
        while (connection.isOpen()) {
            try {
                connection.receive(SocketResponse.class).ifPresent(handler::handle);

            } catch (NoDataReceivedException e) {
                if (connection.isOpen()) {
                    logger.error(format(NO_RESPONSE_FROM, connection.getRemoteAddress()), e);
                } else {
                    logger.error(format(CLOSED_CONNECTION_WITH, connection.getRemoteAddress()), e);
                    throw new ConnectionException(format(CLOSED_CONNECTION_WITH, connection.getRemoteAddress()), e);
                }
            }
        }
    }
}
