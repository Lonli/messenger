package socket.transmitter.response;

import socket.transmitter.sender.ResponseRegistry;
import socket.transmitter.socket.response.SocketResponse;
import socket.transmitter.socket.response.SocketResponseStatus;

public class ClientResponseHandler {

    public void handle(SocketResponse response) {
        String requestId = response.getId();
        ResponseRegistry requestPool = ResponseRegistry.getInstance();
        //ToDo log response status, error if they was, time and other data

        requestPool.put(requestId, response);
    }

    private void handleErrors(SocketResponse response) {
        if (response.getStatus() != SocketResponseStatus.OK) {
            // ToDo error handling
        }
    }
}
