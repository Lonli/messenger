package socket.transmitter.sender;

import socket.transmitter.socket.response.SocketResponse;

import java.util.HashMap;

public class ResponseRegistry
{
    private final HashMap<String, SocketResponse> registry = new HashMap<>();

    public void put(String requestId, SocketResponse response) {
        registry.put(requestId, response);
    }

    public SocketResponse pull(String responseId) {
        if (!registry.containsKey(responseId)) {
            // ToDo throw exception that there is no response for this request
        }

        SocketResponse response = registry.get(responseId);
        registry.remove(responseId);
        return response;
    }

    public boolean has(String responseId) {
        return registry.containsKey(responseId);
    }

    public static ResponseRegistry getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private static class InstanceHolder {
        private static final ResponseRegistry INSTANCE = new ResponseRegistry();
    }
}
