package socket.transmitter.sender;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.connection.Connection;
import socket.transmitter.connection.ConnectionHolder;
import socket.transmitter.exceptions.CannotSendDataException;
import socket.transmitter.exceptions.ConnectionException;
import socket.transmitter.exceptions.ResponseTimeoutException;
import socket.transmitter.security.TokenHolder;
import socket.transmitter.socket.request.SocketRequest;
import socket.transmitter.socket.response.SocketResponse;
import socket.transmitter.utils.JsonConverter;
import socket.transmitter.utils.ThreadLocker;

import java.util.concurrent.TimeoutException;

import static java.lang.String.format;
import static java.util.Objects.isNull;
import static socket.transmitter.exceptions.messages.MessageConstants.CLOSED_CONNECTION_WITH;
import static socket.transmitter.exceptions.messages.MessageConstants.UNESTABLISHED_CONNECTION;

public class Sender<T> {

    private static final Logger logger = LoggerFactory.getLogger(Sender.class);
    public static final int RESPONSE_TIMEOUT_MS = 10000;

    private final SocketRequest request;
    private final Class<T> responseClass;
    private Connection connection;

    public Sender(SocketRequest request) {
        this(request, null);
    }

    public Sender(SocketRequest request, Class<T> responseClass) {
        this.request = request;
        this.responseClass = responseClass;
        prepareSenderConnection();
    }

    public static RequestBuilder createRequest() {
        return new RequestBuilder();
    }

    public T send() throws CannotSendDataException {
        if (connection.isOpen()) {
            connection.send(request);

            if (expectResponse()) {
                return tryToGetResponse();
            } else return null;
        } else {
            logger.error(format(CLOSED_CONNECTION_WITH, connection.getRemoteAddress()));
            throw new ConnectionException(format(CLOSED_CONNECTION_WITH, connection.getRemoteAddress()));
        }
    }

    private T tryToGetResponse() {
        try {
            ResponseRegistry requestPool = ResponseRegistry.getInstance();
            ThreadLocker.waitUntil(() -> requestPool.has(request.getId()), RESPONSE_TIMEOUT_MS);

            SocketResponse socketResponse = requestPool.pull(request.getId());
            return JsonConverter.fromHashMap(socketResponse.getData(), responseClass);
        } catch (TimeoutException e) {
            throw new ResponseTimeoutException("Response timeout.");
        }
    }

    private void prepareSenderConnection() {
        Connection connectionInstance = ConnectionHolder.getInstance().getCollection();

        if (isNull(connectionInstance)) {
            logger.error(UNESTABLISHED_CONNECTION);
            throw new ConnectionException(UNESTABLISHED_CONNECTION);
        }

        this.connection = connectionInstance;
    }

    private boolean expectResponse() {
        return responseClass != null;
    }

    public static class RequestBuilder {

        private final SocketRequest request;

        public RequestBuilder() {
            this.request = new SocketRequest();
            request.setToken(TokenHolder.getInstance().getToken());
        }

        public RequestBuilder withPath(String path) {
            request.setPath(path);
            return this;
        }

        public RequestBuilder withData(Object... data) {
            request.setData(data);
            return this;
        }

        public Sender<Void> prepareRequest() {
            return new Sender<>(request);
        }

        public <R> Sender<R> prepareRequestWithResponse(Class<R> response) {
            return new Sender<>(request, response);
        }
    }
}
