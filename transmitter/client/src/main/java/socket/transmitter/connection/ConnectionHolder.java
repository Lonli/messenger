package socket.transmitter.connection;


public final class ConnectionHolder {

    private Connection collection;

    private ConnectionHolder() {}

    public static ConnectionHolder getInstance() {
        return ConnectionInstanceHolder.instance;
    }

    public synchronized Connection getCollection() {
        return collection;
    }

    public synchronized void setCollection(Connection collection) {
        this.collection = collection;
    }

    private static class ConnectionInstanceHolder {
        private static final ConnectionHolder instance = new ConnectionHolder();
    }
}
