package socket.transmitter.security;

public class TokenHolder {

    private String token;

    public static TokenHolder getInstance() {
        return TokenInstanceHolder.instance;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private static class TokenInstanceHolder {
        private static final TokenHolder instance = new TokenHolder();
    }
}
