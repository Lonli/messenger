package socket.transmitter.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.exceptions.ConnectionPoolIsFullException;
import socket.transmitter.request.ServerRequestListener;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static socket.transmitter.exceptions.messages.MessageConstants.CLOSING_CONNECTIONS;
import static socket.transmitter.exceptions.messages.MessageConstants.CONNECTION_POOL_IS_FULL;

public class ConnectionPool {

    private static final Logger logger = LoggerFactory.getLogger(ConnectionPool.class);

    private final ExecutorService executor;
    private final Connection[] connections;

    public ConnectionPool() {
        //ToDo get number of threads from properties
        this.executor = Executors.newFixedThreadPool(3);
        this.connections = new Connection[3];
    }

    public void add(Connection connection) throws ConnectionPoolIsFullException {
        cleanUpConnections();

        Optional<Integer> index = getConnectionIndex();
        if (index.isPresent()) {
            connections[index.get()] = connection;

            ServerRequestListener requestListener = new ServerRequestListener(connection);
            executor.execute(requestListener);
        } else {
            throw new ConnectionPoolIsFullException(CONNECTION_POOL_IS_FULL);
        }
    }

    public void stopAll() {
        logger.info(CLOSING_CONNECTIONS);
        closeAllConnections();
        executor.shutdown();
    }

    public static ConnectionPool getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private void cleanUpConnections() {
        for (int i = 0; i < connections.length; i++) {
            if (nonNull(connections[i]) && connections[i].isClosed()) {
                connections[i] = null;
            }
        }
    }

    private Optional<Integer> getConnectionIndex() {
        for (int i = 0; i < connections.length; i++) {
            if (isNull(connections[i]) || connections[i].isClosed()) {
                return Optional.of(i);
            }
        }

        return Optional.empty();
    }

    private void closeAllConnections() {
        for (Connection connection: connections) {
            connection.close();
        }
    }

    private static class InstanceHolder {
        private static final ConnectionPool INSTANCE = new ConnectionPool();
    }
}
