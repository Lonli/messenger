package socket.transmitter.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.controller.model.EndpointData;
import socket.transmitter.exceptions.socketExceptions.AccessDeniedException;
import socket.transmitter.exceptions.socketExceptions.InvalidDataException;
import socket.transmitter.exceptions.socketExceptions.NotFoundException;
import socket.transmitter.exceptions.socketExceptions.UnexpectedException;
import socket.transmitter.security.token.TokenGenerator;
import socket.transmitter.socket.request.SocketRequest;
import socket.transmitter.socket.response.SocketResponseStatus;
import socket.transmitter.utils.JsonConverter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static java.lang.String.format;
import static socket.transmitter.exceptions.messages.MessageConstants.*;

public class SocketEndpointHandler {

    private static final Logger logger = LoggerFactory.getLogger(SocketEndpointHandler.class);

    public Object handle(SocketRequest request) throws AccessDeniedException, InvalidDataException, NotFoundException,
            UnexpectedException {
        String token = request.getToken();
        Object[] data = request.getData();
        String path = request.getPath();

        EndpointData endpointData = RequestRouter.route(path);

        boolean isRequiredAuthorization = endpointData.isRequiredAuthorization();
        if (hasAccess(isRequiredAuthorization, token)) {
            return tryToInvoke(endpointData, data);
        } else {
            logger.error(format(ACCESS_DENIED, path));
            throw new AccessDeniedException(SocketResponseStatus.ACCESS_DENIED, format(ACCESS_DENIED, path));
        }
    }

    private boolean hasAccess(boolean isRequiredAuthorization, String token) {
        if (isRequiredAuthorization && !token.isBlank()) {
            return TokenGenerator.validateToken(token);
        } else return !isRequiredAuthorization;
    }

    private Object tryToInvoke(EndpointData endpointData, Object[] data) throws InvalidDataException {
        Object classInstance = endpointData.getInstance();
        Method method = endpointData.getMethod();
        String path = endpointData.getPath();

        try {
            Class<?>[] parameterTypes = endpointData.getMethod().getParameterTypes();
            Object[] dataParams = castToMethodParameters(data, parameterTypes);
            // ToDo test without params casting
            return method.invoke(classInstance, dataParams);
        } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
            logger.error(format(SOCKET_ENDPOINT_INVALID_DATA, path), e);
            throw new InvalidDataException(SocketResponseStatus.BAD_REQUEST, format(SOCKET_ENDPOINT_INVALID_DATA, path));
        }
    }

    private Object[] castToMethodParameters(Object[] data, Class<?>[] paramTypes) throws InvalidDataException {
        if (data.length == paramTypes.length) {
            for (int i = 0; i < data.length; i++) {
                data[i] = JsonConverter.fromHashMap(data[i], paramTypes[i]);
            }
            return data;
        } else {
            logger.error(INVALID_INPUT_DATA);
            throw new InvalidDataException(SocketResponseStatus.BAD_REQUEST, INVALID_INPUT_DATA);
        }
    }
}
