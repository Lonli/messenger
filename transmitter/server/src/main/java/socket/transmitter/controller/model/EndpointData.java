package socket.transmitter.controller.model;

import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Method;

@Getter
@Setter
public class EndpointData {

    private Object instance;
    private Method method;
    private String path;
    private boolean requiredAuthorization;
}
