package socket.transmitter.controller;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.controller.annotation.SocketController;
import socket.transmitter.controller.annotation.SocketEndpoint;
import socket.transmitter.controller.model.EndpointData;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

import static socket.transmitter.exceptions.messages.MessageConstants.SOCKET_CONTROLLER_CREATION_ERROR;

public class SocketControllerSpotter {

    private static final Logger logger = LoggerFactory.getLogger(SocketControllerSpotter.class);

    HashMap<String, EndpointData> loadRoutePaths() {
        Set<Class<?>> classes = new Reflections("", new TypeAnnotationsScanner(), new SubTypesScanner())
                .getTypesAnnotatedWith(SocketController.class, false);
        HashMap<String, EndpointData> requestRegistry = new HashMap<>();

        for (Class<?> clazz : classes) {
            Set<Method> methods = getClassEndpointMethods(clazz);

            for (Method method: methods) {
                try {
                    Object classInstance = clazz.getDeclaredConstructor().newInstance();
                    SocketEndpoint endpoint = method.getAnnotation(SocketEndpoint.class);

                    EndpointData endpointData = new EndpointData();
                    endpointData.setInstance(classInstance);
                    endpointData.setMethod(method);
                    endpointData.setPath(endpoint.path());
                    endpointData.setRequiredAuthorization(endpoint.requiredAuthorization());

                    requestRegistry.put(endpoint.path(), endpointData);
                } catch (InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
                    logger.error(SOCKET_CONTROLLER_CREATION_ERROR, e);
//                    throw new UnexpectedException(SocketResponseStatus.FAILED, SOCKET_CONTROLLER_CREATION_ERROR, e);
                }
            }
        }
        return requestRegistry;
    }

    private Set<Method> getClassEndpointMethods(Class<?> clazz) {
        return Arrays.stream(clazz.getMethods())
                .filter(meth -> meth.isAnnotationPresent(SocketEndpoint.class))
                .collect(Collectors.toSet());
    }
}
