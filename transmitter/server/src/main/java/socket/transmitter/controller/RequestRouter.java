package socket.transmitter.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.controller.model.EndpointData;
import socket.transmitter.exceptions.socketExceptions.NotFoundException;
import socket.transmitter.socket.response.SocketResponseStatus;

import java.util.HashMap;

import static java.lang.String.format;
import static socket.transmitter.exceptions.messages.MessageConstants.NOT_FOUND_ENDPOINT;

public class RequestRouter {

    private static final Logger logger = LoggerFactory.getLogger(RequestRouter.class);
    private static final HashMap<String, EndpointData> ROUTE_REGISTRY = new SocketControllerSpotter().loadRoutePaths();

    private RequestRouter() {
    }

    static EndpointData route(String path) throws NotFoundException {
        if (ROUTE_REGISTRY.containsKey(path)) {
            return ROUTE_REGISTRY.get(path);
        } else {
            logger.error(format(NOT_FOUND_ENDPOINT, path));
            throw new NotFoundException(SocketResponseStatus.NOT_FOUND, format(NOT_FOUND_ENDPOINT, path));
        }
    }

}
