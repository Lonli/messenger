package socket.transmitter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.connection.Connection;
import socket.transmitter.connection.ConnectionPool;
import socket.transmitter.exceptions.ConnectionPoolIsFullException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import static java.lang.String.format;
import static socket.transmitter.exceptions.messages.MessageConstants.*;

public class ServerSocketStarter {

    private static final Logger logger = LoggerFactory.getLogger(ServerSocketStarter.class);

    public void start(int port) {
        logger.info(SERVER_STARTED);
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            logger.info(OPENING_CONNECTION_PORT);
            while(true) {
                logger.info(WAITING_FOR_CONNECTION);

                Socket socket = serverSocket.accept();
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Connection connection = new Connection(socket, inputStream, outputStream);

                logger.info(format(CLIENT_TRYING_TO_CONNECT, connection.getRemoteAddress()));
                //ToDo change method name
                tryAddToPool(connection);
            }

        } catch (IOException e) {
            logger.error(UNEXPECTED_ERROR, e);
        }
    }

    private void tryAddToPool(Connection connection) {
        try {
            ConnectionPool.getInstance().add(connection);
            logger.info(format(CLIENT_CONNECTED_TO_THE_SERVER, connection.getRemoteAddress()));
        }catch (ConnectionPoolIsFullException e) {
            logger.error(CONNECTION_POOL_IS_FULL, e);
            logger.info(format(CLOSING_CONNECTION_WITH, connection.getRemoteAddress()));
            connection.close();
        }
    }
}
