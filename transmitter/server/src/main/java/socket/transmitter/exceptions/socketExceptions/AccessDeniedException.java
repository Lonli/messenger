package socket.transmitter.exceptions.socketExceptions;

import socket.transmitter.socket.response.SocketResponseStatus;

public class AccessDeniedException extends SocketException {

    public AccessDeniedException(SocketResponseStatus status, String message) {
        super(status, message);
    }

    public AccessDeniedException(SocketResponseStatus status, String message, Throwable throwable) {
        super(status, message, throwable);
    }
}
