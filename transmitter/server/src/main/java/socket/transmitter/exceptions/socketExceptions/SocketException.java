package socket.transmitter.exceptions.socketExceptions;

import lombok.Getter;
import socket.transmitter.socket.response.SocketResponseStatus;

@Getter
public class SocketException extends Throwable {

    private final SocketResponseStatus status;

    public SocketException(SocketResponseStatus status, String message) {
        super(message);
        this.status = status;
    }

    public SocketException(SocketResponseStatus status, String message, Throwable throwable) {
        super(message, throwable);
        this.status = status;
    }
}
