package socket.transmitter.exceptions;

public class ConnectionPoolIsFullException extends Throwable {

    public ConnectionPoolIsFullException(String message) {
        super(message);
    }

    public ConnectionPoolIsFullException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
