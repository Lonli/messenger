package socket.transmitter.exceptions.socketExceptions;

import socket.transmitter.socket.response.SocketResponseStatus;

public class NotFoundException extends SocketException {

    public NotFoundException(SocketResponseStatus status, String message) {
        super(status, message);
    }

    public NotFoundException(SocketResponseStatus status, String message, Throwable throwable) {
        super(status, message, throwable);
    }

}
