package socket.transmitter.exceptions.socketExceptions;

import socket.transmitter.socket.response.SocketResponseStatus;

public class UnexpectedException extends SocketException {

    public UnexpectedException(SocketResponseStatus status, String message) {
        super(status, message);
    }

    public UnexpectedException(SocketResponseStatus status, String message, Throwable throwable) {
        super(status, message, throwable);
    }

}
