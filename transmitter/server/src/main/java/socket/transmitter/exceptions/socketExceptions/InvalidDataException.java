package socket.transmitter.exceptions.socketExceptions;

import socket.transmitter.socket.response.SocketResponseStatus;

public class InvalidDataException extends SocketException {

    public InvalidDataException(SocketResponseStatus status, String message) {
        super(status, message);
    }

    public InvalidDataException(SocketResponseStatus status, String message, Throwable throwable) {
        super(status, message, throwable);
    }
}
