package socket.transmitter.security.token;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import socket.transmitter.security.AuthorizationUser;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class TokenGenerator {

    public static final String TOKEN_ISSUER = "http://localhost:8080/";
    public static final String TOKEN_SUBJECT = "subject";
    public static final String TOKEN_USERID_CLAIM = "userId";
    public static final String TOKEN_KEY = "this is a secured secret key";
    public static final byte[] TOKEN_BYTE_KEY = toBytes(TOKEN_KEY, 255);


    private TokenGenerator() {
    }

    public static String generateToken(long userId) {
        Date date = Date.from(Instant.now().plus(1, ChronoUnit.HOURS));

        return Jwts.builder()
                .setIssuer(TOKEN_ISSUER)
                .setSubject(TOKEN_SUBJECT)
                .claim(TOKEN_USERID_CLAIM, userId)
                .setExpiration(date)
                .signWith(SignatureAlgorithm.HS256, TOKEN_BYTE_KEY)
                .setExpiration(date)
                .compact();
    }

    public static boolean validateToken(String token) {
        return Jwts.parserBuilder().setSigningKey(TOKEN_BYTE_KEY)
                .requireIssuer(TOKEN_ISSUER)
                .requireSubject(TOKEN_SUBJECT)
                .require(TOKEN_USERID_CLAIM, AuthorizationUser.class)
                .build()
                .isSigned(token);
    }

    public static AuthorizationUser deserializeToken(String token) {
        Object userObj = Jwts.parserBuilder()
                .build()
                .parseClaimsJwt(token)
                .getBody()
                .get(TOKEN_USERID_CLAIM);

        return (AuthorizationUser) userObj;
    }

    private static byte[] toBytes(String data, int length) {
        byte[] result = new byte[length];
        System.arraycopy(data.getBytes(), 0, result, length - data.length(), data.length());
        return result;
    }
}
