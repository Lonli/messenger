package socket.transmitter.security;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthorizationUser {
    private long id;
}
