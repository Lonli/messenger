package socket.transmitter.request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.connection.Connection;
import socket.transmitter.exceptions.ConnectionException;
import socket.transmitter.exceptions.NoDataReceivedException;
import socket.transmitter.socket.request.SocketRequest;

import static java.lang.String.format;
import static socket.transmitter.exceptions.messages.MessageConstants.CLOSED_CONNECTION_WITH;

public class ServerRequestListener implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(ServerRequestListener.class);

    private final Connection connection;
    private final SocketRequestHandler handler;

    public ServerRequestListener(Connection connection) {
        this.connection = connection;
        this.handler = new SocketRequestHandler(connection); // ToDo dependency injection;
    }

    @Override
    public void run() {
        while (connection.isOpen()) {
            try {
                connection.receive(SocketRequest.class).ifPresent(handler::handle);
            } catch (NoDataReceivedException e) {
                if (connection.isClosed()) {
                    logger.error(format(CLOSED_CONNECTION_WITH, connection.getRemoteAddress()), e);
                    throw new ConnectionException(format(CLOSED_CONNECTION_WITH, connection.getRemoteAddress()), e);
                }
            }
        }
    }
}
