package socket.transmitter.request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.connection.Connection;
import socket.transmitter.exceptions.CannotSendDataException;
import socket.transmitter.exceptions.ConnectionException;
import socket.transmitter.socket.response.SocketResponse;
import socket.transmitter.socket.response.SocketResponseStatus;

import static java.lang.String.format;
import static socket.transmitter.exceptions.messages.MessageConstants.CLOSED_CONNECTION_WITH;
import static socket.transmitter.exceptions.messages.MessageConstants.FAILED_SENDING_RESPONSE_TO;

public class Responder {

    private static final Logger logger = LoggerFactory.getLogger(Responder.class);

    private final SocketResponse response;
    private final Connection connection;

    Responder(SocketResponse response,
                     Connection connection) {
        this.connection = connection;
        this.response = response;
    }

    void respond() {
        try {
            if (connection.isOpen()) {
                connection.send(response);
            } else {
                logger.error(format(CLOSED_CONNECTION_WITH, connection.getRemoteAddress()));
                throw new ConnectionException(format(CLOSED_CONNECTION_WITH, connection.getRemoteAddress()));
            }
        } catch (CannotSendDataException e) {
            logger.error(format(FAILED_SENDING_RESPONSE_TO, connection.getRemoteAddress()), e);
        }
    }

    static ResponseBuilder createResponse(Connection connection) {
        return new ResponseBuilder(connection);
    }

    static class ResponseBuilder {

        private final SocketResponse response = new SocketResponse();
        private final Connection connection;

        public ResponseBuilder(Connection connection) {
            this.connection = connection;
        }

        public ResponseBuilder withId(String id) {
            response.setId(id);
            return this;
        }

        public ResponseBuilder withStatus(SocketResponseStatus status) {
            response.setStatus(status);
            return this;
        }

        public ResponseBuilder withData(Object data) {
            response.setData(data);
            return this;
        }

        public Responder prepareResponse() {
            return new Responder(response, connection);
        }

    }
}
