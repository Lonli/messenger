package socket.transmitter.request;

import socket.transmitter.connection.Connection;
import socket.transmitter.controller.SocketEndpointHandler;
import socket.transmitter.exceptions.socketExceptions.SocketException;
import socket.transmitter.socket.request.SocketRequest;
import socket.transmitter.socket.response.SocketResponseStatus;

public class SocketRequestHandler {

    private final Connection connection;
    private final SocketEndpointHandler handler;
    public SocketRequestHandler(Connection connection) {
        this.connection = connection;
        this.handler = new SocketEndpointHandler(); //ToDo dependency injection
    }

    public void handle(SocketRequest request) {
        String id = request.getId();

        try {
            Object result = handler.handle(request);
            Responder.createResponse(connection)
                    .withId(id)
                    .withData(result)
                    .withStatus(SocketResponseStatus.OK)
                    .prepareResponse()
            .respond();

        } catch (SocketException e) {
            Responder.createResponse(connection)
                    .withId(id)
                    .withStatus(e.getStatus())
                    .withData(e.getMessage())
                    .prepareResponse()
            .respond();
        }
    }
}
