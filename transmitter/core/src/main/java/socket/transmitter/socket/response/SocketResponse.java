package socket.transmitter.socket.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SocketResponse {

    private String id;
    private Object data;
    private SocketResponseStatus status;

    public boolean hasData() {
        return nonNull(data);
    }
}
