package socket.transmitter.socket.response;

public enum SocketResponseStatus {
    OK,
    FAILED,
    BAD_REQUEST,
    ACCESS_DENIED,
    NOT_FOUND
}
