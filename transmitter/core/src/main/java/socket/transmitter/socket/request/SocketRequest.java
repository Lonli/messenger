package socket.transmitter.socket.request;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class SocketRequest {

    private String id;
    private Object[] data;
    private String token;
    private String path;

    public SocketRequest() {
        this.id = UUID.randomUUID().toString();
    }
}
