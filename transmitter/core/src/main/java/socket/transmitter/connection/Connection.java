package socket.transmitter.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import socket.transmitter.exceptions.CannotSendDataException;
import socket.transmitter.exceptions.NoDataReceivedException;
import socket.transmitter.utils.JsonConverter;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Optional;

import static java.lang.String.format;
import static socket.transmitter.exceptions.messages.MessageConstants.*;

public class Connection {

    private static final Logger logger = LoggerFactory.getLogger(Connection.class);

    private final Socket socket;
    private final ObjectInputStream inputStream;
    private final ObjectOutputStream outputStream;

    public Connection(Socket socket, ObjectInputStream inputStream, ObjectOutputStream outputStream) {
        this.socket = socket;
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    public void send(Object object) throws CannotSendDataException {
        try {
            String json = JsonConverter.toJson(object);
            outputStream.writeObject(json);
            outputStream.flush();
        } catch (IOException e) {
            logger.error(format(FAILED_SENDING_REQUEST_TO, getRemoteAddress()), e);
            throw new CannotSendDataException(format(FAILED_SENDING_REQUEST_TO, getRemoteAddress()), e);
        }
    }

    public <C> Optional<C> receive(Class<C> clazz) throws NoDataReceivedException {
        try {
            Object object = inputStream.readObject();

            if (object instanceof String) {
                String json = (String) object;
                return JsonConverter.fromJson(json, clazz);
            }
        } catch (IOException e) {
            // ToDo return Optional.empty()?
//            logger.error(format(NO_DATA_RECEIVED_FROM, getRemoteAddress()), e);
//            throw new NoDataReceivedException(format(NO_DATA_RECEIVED_FROM, getRemoteAddress()), e);
        } catch (ClassNotFoundException ignore) {
            // ToDo log that incorrect data was received;
        }

        return Optional.empty();
    }

    public void close() {
        try {
            logger.info(format(CLOSING_CONNECTION_BETWEEN, getRemoteAddress(), getAddress()));
            inputStream.close();
            outputStream.close();
            socket.close();
            logger.info(format(CLOSED_CONNECTION_WITH, getRemoteAddress()));
        } catch (IOException e) {
            logger.error(format(CLOSING_CONNECTION_FAILED, getRemoteAddress(), getAddress()), e);
        }
    }

    public InetAddress getAddress() {
        return socket.getLocalAddress();
    }

    public InetAddress getRemoteAddress() {
        return socket.getInetAddress();
    }

    public boolean isOpen() {
        return !socket.isClosed();
    }

    public boolean isClosed() {
        return socket.isClosed();
    }

    @Override
    protected void finalize() throws Throwable {
        if (isOpen()) close();
    }
}
