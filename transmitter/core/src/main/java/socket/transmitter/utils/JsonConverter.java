package socket.transmitter.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.util.Optional;

public class JsonConverter {

    private static final ObjectMapper mapper;

    static {
        mapper = new ObjectMapper().registerModule(new JavaTimeModule());
    }

    private JsonConverter() {
    }

    public static String toJson(Object object) throws IOException {
        return mapper.writeValueAsString(object);
    }

    public static <C> Optional<C> fromJson(String json, Class<C> clazz) {
        try {
            return Optional.of(mapper.readValue(json, clazz));
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    public static <C> C fromHashMap(Object object, Class<C> clazz) {
        return mapper.convertValue(object, clazz);
    }

    public static <C> C fromHashMap(Object[] object, Class<C> clazz) {
        return mapper.convertValue(object, clazz);
    }
}
