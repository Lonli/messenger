package socket.transmitter.utils;

import java.util.concurrent.TimeoutException;
import java.util.function.BooleanSupplier;

public class ThreadLocker {

    public static void waitUntil(BooleanSupplier condition, long timeMS) throws TimeoutException {
        long timeOutMillis = calculateTimeOut(timeMS);

        while (!condition.getAsBoolean()) {
            if (System.currentTimeMillis() > timeOutMillis) {
                throw new TimeoutException();
            }
        }
    }

    private static long calculateTimeOut(long timeMS) {
        long currentTime = System.currentTimeMillis();
        return currentTime + timeMS;
    }
}
