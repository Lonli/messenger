package socket.transmitter.exceptions;

public class NoDataReceivedException extends Throwable {

    public NoDataReceivedException(String message) {
        super(message);
    }

    public NoDataReceivedException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
