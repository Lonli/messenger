package socket.transmitter.exceptions;

public class CannotSendDataException extends Throwable {

    public CannotSendDataException(String message) {
        super(message);
    }

    public CannotSendDataException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
