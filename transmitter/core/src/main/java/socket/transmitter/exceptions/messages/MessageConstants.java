package socket.transmitter.exceptions.messages;

public class MessageConstants {

    public static final String CLIENT_STARTED = "Client started successfully";
    public static final String CONNECTING_TO_THE_SERVER = "Establishing a connection to the server ...";
    public static final String CONNECTION_TO_THE_SERVER_COMPLETED = "Connection to the server completed successfully";

    public static final String SERVER_STARTED = "Server started successfully";
    public static final String OPENING_CONNECTION_PORT = "Opening connection port.";
    public static final String WAITING_FOR_CONNECTION = "Waiting for a new connection.";
    public static final String CLIENT_TRYING_TO_CONNECT = "Client [ %s ] is trying to connect.";
    public static final String CLIENT_CONNECTED_TO_THE_SERVER = "Client [ %s ] successfully connected to the sever";

    public static final String UNESTABLISHED_CONNECTION = "Unestablished connection.";

    public static final String CONNECTION_IS_CLOSED = "Connection is closed.";
    public static final String CLOSED_CONNECTION_WITH = "Connection with [ %s ] is closed.";

    public static final String CLOSING_CONNECTIONS = "Closing connections...";
    public static final String CLOSING_CONNECTION_WITH = "Closing connection with [ %s ]";
    public static final String CLOSING_CONNECTION_BETWEEN = "Closing connection between server [ %s ] and client [ %s ].";
    public static final String CLOSING_CONNECTION_FAILED = "Failed to close connection between server [ %s ] and client [ %s ].";

    public static final String NO_RESPONSE_FROM = "No response from [ %s ].";
    public static final String NO_DATA_RECEIVED_FROM = "No data received from [ %s ].";
    public static final String INVALID_RESPONSE_FROM = "Invalid response object receiver from [ %s ].";
    public static final String INVALID_REQUEST_FROM = "Invalid request object receiver from [ %s ].";

    public static final String FAILED_SENDING_REQUEST_TO = "Sending a request to [ %s ] is failed.";
    public static final String FAILED_SENDING_RESPONSE_TO = "Sending a response to [ %s ] is failed.";

    public static final String SOCKET_CONTROLLER_CREATION_ERROR = "Can not create socket controller instance.";
    public static final String SOCKET_ENDPOINT_INVALID_DATA = "Invalid data for socket endpoint with path [ %s ].";

    public static final String CONNECTION_POOL_IS_FULL = "Connection pool is full.";
    public static final String CONNECTION_REFUSED = "Server connection rejected";

    public static final String ACCESS_DENIED = "User does not have access to %s endpoint.";
    public static final String NOT_FOUND_ENDPOINT = "No such endpoint with path [ %s ].";
    public static final String UNEXPECTED_ERROR = "Unexpected Error!";

    public static final String INVALID_INPUT_DATA = "Invalid parameters passed in request";

    private MessageConstants() {}
}
