package org.chstu.messenger.entity.messsage;

import lombok.Getter;
import lombok.Setter;
import org.chstu.messenger.entity.user.User;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
public class Message {

    private String messageId;
    private String content;
    private User author;
    private Instant created;
    private boolean isEdited;

    public Message(String content, User author) {
        this.messageId = "message_" + UUID.randomUUID();
        this.content = content;
        this.author = author;
        this.created = Instant.now();
    }
}
