package org.chstu.messenger.entity.chat;

import lombok.Getter;
import lombok.Setter;
import org.chstu.messenger.entity.messsage.Message;
import org.chstu.messenger.entity.user.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class Chat {

    private long chatId;
    private User[] participants;
    private List<Message> messages;
    private long g, p, publicKey;

    public Chat() {
        this.messages = new ArrayList<>();
    }

    public void addMessage(Message message) {
        messages.add(message);
    }

    public User[] getParticipantsExcept(long userId) {
        return Arrays.stream(participants)
                .filter(part -> part.getId() != userId)
                .toArray(User[]::new);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chat chat = (Chat) o;
        return chatId == chat.chatId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(chatId);
    }
}
