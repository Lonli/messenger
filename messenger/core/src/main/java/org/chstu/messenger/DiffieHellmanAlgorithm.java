package org.chstu.messenger;

import org.chstu.messenger.entity.user.User;

public class DiffieHellmanAlgorithm implements ExchangeAlgorithm {

    @Override
    public long calcKey(long p, long pubKey, long prvKey) {
        return (long) Math.pow(pubKey, prvKey) % p;

    }

    @Override
    public long calcChatPublicKey(long g, long p, User... participants) {
        long pubKey = g;

        for (User user : participants) {
            pubKey = (long) Math.pow(pubKey, user.getPrivateKey());
        }

        return pubKey;
    }
}
