package org.chstu.messenger.utils;

import org.chstu.messenger.exceptions.MissingPropertiesFileException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class PropertyLoader {
    private final Properties properties = new Properties();

    public Properties load(String propertyName) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertyName)) {

            if (inputStream != null) {
                properties.load(new InputStreamReader(inputStream, "windows-1251"));

                return properties;
            } else {
                throw new MissingPropertiesFileException("File '" + propertyName + "' is missing!");
            }
        } catch (IOException e) {
            throw new MissingPropertiesFileException("File '" + propertyName + "' is missing!");
        }
    }
}
