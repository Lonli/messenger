package org.chstu.messenger.entity.user;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LoginMapper {
    LoginMapper INSTANCE = Mappers.getMapper(LoginMapper.class);

    LoginDto toDto(Login login);

    Login toEntity(LoginDto dto);
}
