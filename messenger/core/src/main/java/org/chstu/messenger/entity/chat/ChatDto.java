package org.chstu.messenger.entity.chat;

import lombok.Getter;
import lombok.Setter;
import org.chstu.messenger.entity.messsage.Message;
import org.chstu.messenger.entity.user.User;

import java.util.List;

@Getter
@Setter
public class ChatDto {

    private long chatId;
    private User[] participants;
    private List<Message> messages;
    private long g, p, publicKey;
}
