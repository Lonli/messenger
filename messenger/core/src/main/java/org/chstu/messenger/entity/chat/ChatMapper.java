package org.chstu.messenger.entity.chat;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ChatMapper {
    ChatMapper INSTANCE = Mappers.getMapper(ChatMapper.class);

    ChatDto toDto(Chat chat);

    Chat toEntity(ChatDto dto);
}
