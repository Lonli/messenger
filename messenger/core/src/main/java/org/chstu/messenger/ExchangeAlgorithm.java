package org.chstu.messenger;

import org.chstu.messenger.entity.user.User;

public interface ExchangeAlgorithm {

    long calcKey(long p, long pubKey, long prvKey);

    long calcChatPublicKey(long g, long p, User... participants);
}
