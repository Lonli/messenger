package org.chstu.messenger.entity.messsage;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.chstu.messenger.entity.user.User;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
public class MessageDto {

    private String messageId;
    private String content;
    private User author;
    private Instant created;
    private boolean isEdited;

}
