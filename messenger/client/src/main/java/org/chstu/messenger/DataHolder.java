package org.chstu.messenger;

import lombok.Getter;
import lombok.Setter;
import org.chstu.messenger.entity.Chat;
import org.chstu.messenger.entity.User;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DataHolder {

    private final List<Chat> chats = new ArrayList<>();
    private User user;

    public Chat getChat(long chatId) {
        //ToDo do not use get without validation
        return chats.stream().filter(chat -> chat.getChatId() == chatId).findFirst().get();
    }

    public void add(Chat chat) {
        chats.add(chat);
    }

    public static DataHolder getInstance() {
        return DataHolderInstanceHolder.instance;
    }

    private static class DataHolderInstanceHolder {

        public static final DataHolder instance = new DataHolder();

    }
}
