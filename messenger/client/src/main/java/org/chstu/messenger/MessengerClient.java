package org.chstu.messenger;

import socket.transmitter.ClientSocketStarter;

public class MessengerClient {
    public static void main(String[] args) {
        new ClientSocketStarter().start("localhost", 8080);
    }
}