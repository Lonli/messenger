package org.chstu.messenger;

import org.chstu.messenger.entity.dto.LoginDto;
import socket.transmitter.exceptions.CannotSendDataException;
import socket.transmitter.security.TokenHolder;
import socket.transmitter.sender.Sender;

public class LoginController {

    public void login(String username, String password) {
        LoginDto loginDto = new LoginDto();
        loginDto.setUsername(username);
        loginDto.setPassword(password);

        try {
            String token = Sender.createRequest()
                    .withPath("/login")
                    .withData(loginDto)
                    .prepareRequestWithResponse(String.class)
                    .send();

            System.out.println(token);
            //ToDo add some validation
            TokenHolder.getInstance().setToken(token);
        } catch (CannotSendDataException e) {
            // ToDo write login failed
        }
    }
}
