package org.chstu.messenger;

import org.chstu.messenger.entity.Chat;
import org.chstu.messenger.entity.User;
import org.chstu.messenger.entity.dto.MessageDto;
import socket.transmitter.exceptions.CannotSendDataException;
import socket.transmitter.sender.Sender;

public class MessageController {

    public void send(String text, long chatId) {
        String encodedText = getEncodedText(text, chatId);

        MessageDto message = new MessageDto();
        message.setContent(encodedText);

        try {
            Sender.createRequest()
                    .withPath("/sendMessage")
                    .withData(message)
                    .prepareRequest()
                    .send();
        } catch (CannotSendDataException e) {
            // ToDo Show error message and repeat sending after some period of time
        }
    }

    private String getEncodedText(String text, long chatId) {
        Chat chat = DataHolder.getInstance().getChat(chatId);
        long p = chat.getP();
        long chatPubKey = chat.getPublicKey();
        User user = DataHolder.getInstance().getUser();
        long prvKey = user.getPrivateKey();
        long encodeKey = new DiffieHellmanAlgorithm().calcKey(p, chatPubKey, prvKey);
        return text + encodeKey; //ToDo encode functionality;
    }
}
