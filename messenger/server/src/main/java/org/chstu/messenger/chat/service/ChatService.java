package org.chstu.messenger.chat.service;

import org.chstu.messenger.entity.chat.Chat;
import org.chstu.messenger.entity.chat.ChatDto;

public interface ChatService {

    ChatDto get(long chatId, long userId);

    ChatDto create(String name, long... userId);

    ChatDto save(Chat chat);

    ChatDto delete(long chatId);

    void changeName(String name);

    boolean hasAccess(long userId, long chatId);
}
