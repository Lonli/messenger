package org.chstu.messenger.chat.service;

import org.chstu.messenger.DiffieHellmanAlgorithm;
import org.chstu.messenger.ExchangeAlgorithm;
import org.chstu.messenger.entity.chat.Chat;
import org.chstu.messenger.entity.chat.ChatDto;
import org.chstu.messenger.entity.chat.ChatMapper;

public class ChatServiceImpl implements ChatService {

    private final ExchangeAlgorithm algorithm = new DiffieHellmanAlgorithm();
    private final ChatMapper mapper = ChatMapper.INSTANCE;

    @Override
    public ChatDto get(long chatId, long userId) {

        return new ChatDto();
    }

    @Override
    public ChatDto create(String name, long... userId) {
        return null;
    }

    @Override
    public ChatDto save(Chat chat) {
        return null;
    }

    @Override
    public ChatDto delete(long chatId) {
        return null;
    }

    @Override
    public void changeName(String name) {

    }

    @Override
    public boolean hasAccess(long userId, long chatId) {
        return false;
    }
}
