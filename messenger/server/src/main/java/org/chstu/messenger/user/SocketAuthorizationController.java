package org.chstu.messenger.user;

import org.chstu.messenger.entity.user.LoginDto;
import org.chstu.messenger.entity.user.User;
import org.chstu.messenger.user.service.UserService;
import org.chstu.messenger.user.service.UserServiceImpl;
import socket.transmitter.controller.annotation.SocketController;
import socket.transmitter.controller.annotation.SocketEndpoint;
import socket.transmitter.security.token.TokenGenerator;


@SocketController
public class SocketAuthorizationController {

    private final UserService userService;

    public SocketAuthorizationController() {
        this.userService = new UserServiceImpl();
    }

    @SocketEndpoint(path = "/login", requiredAuthorization = false)
    public String login(LoginDto login) {
        User user = userService.getByCredentials(login.getUsername(), login.getPassword());

        return TokenGenerator.generateToken(user.getId());
    }
}
