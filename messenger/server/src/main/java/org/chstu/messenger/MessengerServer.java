package org.chstu.messenger;

import socket.transmitter.ServerSocketStarter;

public class MessengerServer {

    public static void main(String[] args) {
        new ServerSocketStarter().start(8080);
    }
}
