package org.chstu.messenger.user.service;

import org.chstu.messenger.entity.user.User;

public interface UserService {

    User getByCredentials(String username, String password);
}
