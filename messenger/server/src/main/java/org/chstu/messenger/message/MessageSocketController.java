package org.chstu.messenger.message;

import org.chstu.messenger.entity.messsage.MessageDto;
import org.chstu.messenger.entity.messsage.SendingStatus;
import socket.transmitter.controller.annotation.SocketController;
import socket.transmitter.controller.annotation.SocketEndpoint;

@SocketController
public class MessageSocketController {


    @SocketEndpoint(path = "/sendMessage", requiredAuthorization = false)
    public SendingStatus sendMessage(MessageDto messageDto) {
        //ToDo save to db
        System.out.println("I ve receive a new message! " + messageDto.getContent());
        return SendingStatus.SENT;
    }
}
